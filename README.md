# gauss.tex
This .tex file contains an environment which makes writing gauss matrices more easier.
This is not extensively tested so it might contain bugs and is far from being perfect!!!

It was written with the help of the tex.stackoverflow users
- [Schrödinger's cat](https://tex.stackexchange.com/users/194703/schr%c3%b6dingers-cat)
- [F. Pantigny](https://tex.stackexchange.com/users/163000/f-pantigny)
- [muzimuzhi Z](https://tex.stackexchange.com/users/79060/muzimuzhi-z)

Thank you!

I intend to update this when the new nicematrix version has reached me (I'm still waiting for the new latex package)
and I'm hoping that at least then I'm able to remove the global macro definition.

I'm new to tex macro/environment writing, so if someone has any hints, advice or suggestions,
be welcome to notify me (via gitlab issues or via mail at mail-login+gitlab@protonmail.com
